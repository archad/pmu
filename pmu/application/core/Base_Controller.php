<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Base_Controller extends CI_Controller {

    Public function __construct() {
        parent::__construct();
        if (empty($this->session->userdata('lang'))) {
            $this->session->set_userdata('lang', "french");
        }
        $this->lang->load('menu', $this->session->userdata('lang'));
    }

    /* public function verifSession(){
      try {
      if(empty($this->session->userdata('idUser'))){
      redirect(base_url('index.php/login'));
      }
      } catch (Exception $exc) {
      echo $exc->getMessage();
      }
      }

      public function verifUserType($allow){
      try {
      $typeUser=$this->session->userdata('typeUser');
      if ($typeUser!=$allow) {
      switch ($typeUser) {
      case 1:
      redirect(base_url('index.php/AccueilAdmin'));
      default:
      redirect(base_url('index.php/Accueil'));
      break;
      }
      }
      } catch (Exception $exc) {
      echo $exc->getMessage();
      }
      } */
}
