<?php
/**
 * Created by PhpStorm.
 * User: josu
 * Date: 19/09/2019
 * Time: 14:30
 */

class ProgrammeResultat extends Base_Controller
{
    public function index(){
        $data = array(
            "page" => "ProgrammeResultat_index"
        );
        $this->load->view("template", $data);
    }

    public function goToDetailCourse(){
        $data = array(
            "page" => "detailCourse"
        );
        $this->load->view("template", $data);
    }
}