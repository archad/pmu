<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Accueil extends Base_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index() {
        $today = date("d/m/Y");
        $data = array(
            "page" => "Accueil_index",
            "d_today" => $today,
            "d_yesterday" => date('d/m/Y', strtotime("-1 day")),
            "d_tomorrow" => date('d/m/Y', strtotime("+1 day"))
        );
        $this->load->view('template', $data);
    }

    public function changeLang($lang,$url="accueil") {
        $this->session->set_userdata('lang', $lang);
        redirect($url);
    }

}
