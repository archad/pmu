<?php /**
 * Created by PhpStorm.
 * User: josu
 * Date: 18/09/2019
 * Time: 13:25
 */ ?>

<nav class="navbar navbar-expand-lg navbar-dark bg-pmu-green fixed-top material-shadow" style="padding: 0">
    <div class="container">
        <a class="navbar-brand" href="<?= base_url() ?>"><img src="assets/img/logo.png" alt="PMU Madagascar LOGO"></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive" style="margin: 0.5rem 1rem;">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url() ?>"><?= lang('menu1') ?></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('/programme-et-resultat') ?>"><?= lang('menu2') ?></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('/comment-jouer') ?>"><?= lang('menu3') ?></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('/contacts') ?>"><?= lang('menu5') ?></a>
                </li>
                <li class="nav-item dropdown">
                    
                    <!--<div class="btn-group-sm">
                        <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown">
                            Sony
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="#">Tablet</a>
                            <a class="dropdown-item" href="#">Smartphone</a>
                        </div>
                    </div>-->

                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Language
                    </a>
                    <div class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarDropdownBlog">
                        <a class="dropdown-item" href="<?= base_url('accueil/changeLang/french/'.uri_string()) ?>">Français</a>
                        <a class="dropdown-item" href="<?= base_url('accueil/changeLang/english/'.uri_string()) ?>">Anglais</a>
                        <a class="dropdown-item" href="<?= base_url('accueil/changeLang/malagasy/'.uri_string()) ?>">Malagasy</a>
                    </div>

                </li>
            </ul>
        </div>
    </div>
</nav>
