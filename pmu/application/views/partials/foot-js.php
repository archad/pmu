<?php /**
 * Created by PhpStorm.
 * User: josu
 * Date: 18/09/2019
 * Time: 13:25
 */ ?>

<!-- Bootstrap core JavaScript -->
<script src="assets/vendor/jquery/jquery.min.js"></script>
<script src="assets/vendor/mdb/js/popper.min.js"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="assets/vendor/parallax/parallax.min.js"></script>
<script src="assets/vendor/timetable/js/timetable.js"></script>
<script src="assets/vendor/mdb/js/mdb.min.js"></script>
<script>
    let timetable = new Timetable();
    timetable.setScope(12, 20);
    timetable.addLocations(['R1 - MAISONS-LAFFITTE', 'R2 - COMPIEGNE', 'R3 - VAAL (ZAF)', 'R4 - VINCENNES']);
    timetable.addEvent(
        '<div title="C1 - BRIONNAIS - 13:55" style="height:60%;" class="d-flex flex-column justify-content-center">' +
        '   <div class="row d-flex justify-content-center">' +
        '       <div class="col-5">C1</div>' +
        '   </div>' +
        '   <div class="row d-flex justify-content-center">' +
        '       <div class="col-9">BRIONNAIS</div>' +
        '   </div>' +
        '</div>' +
        '<div style="height: 40%; background: white;" class="d-flex flex-column justify-content-center">' +
        '   <div class="row d-flex justify-content-center">' +
        '       <div class="col-12"><span class="pmu-text-red">Arrivée définitive</span><br><span class="text-black">7-12-6-8-13</span></div>' +
        '   </div>' +
        '</div>',
        'R1 - MAISONS-LAFFITTE', new Date(2019, 10, 2, 13, 55), new Date(2019, 10, 2, 14, 25));
    timetable.addEvent(
        '<div title="C2 - ESMERALDA - 14:27" style="height:60%;" class="d-flex flex-column justify-content-center">' +
        '   <div class="row d-flex justify-content-center">' +
        '       <div class="col-5">C2</div>' +
        '   </div>' +
        '   <div class="row d-flex justify-content-center">' +
        '       <div class="col-9">ESMERALDA</div>' +
        '   </div>' +
        '</div>' +
        '<div style="height: 40%; background: white;" class="d-flex flex-column justify-content-center">' +
        '   <div class="row d-flex justify-content-center">' +
        '       <div class="col-12"><span class="pmu-text-red">Arrivée définitive</span><br><span class="text-black">3-6-2-5-1</span></div>' +
        '   </div>' +
        '</div>',
        'R1 - MAISONS-LAFFITTE', new Date(2019, 10, 2, 14, 27), new Date(2019, 10, 2, 14, 57));
    timetable.addEvent(
        '<div title="C3 - NINO - 15:01" style="height:60%;" class="d-flex flex-column justify-content-center">' +
        '   <div class="row d-flex justify-content-center">' +
        '       <div class="col-5">C3</div>' +
        '   </div>' +
        '   <div class="row d-flex justify-content-center">' +
        '       <div class="col-9">NINO</div>' +
        '   </div>' +
        '</div>' +
        '<div style="height: 40%; background: white;" class="d-flex flex-column justify-content-center">' +
        '   <div class="row d-flex justify-content-center">' +
        '       <div class="col-12"><span class="pmu-text-red">Arrivée définitive</span><br><span class="text-black">3-4-1-6-2</span></div>' +
        '   </div>' +
        '</div>',
        'R1 - MAISONS-LAFFITTE', new Date(2019, 10, 2, 15, 1), new Date(2019, 10, 2, 15, 31));
    let renderer = new Timetable.Renderer(timetable);
    renderer.draw('#results');
    let rend = new Timetable.Renderer(timetable);
    rend.draw('#today');
    let rend2 = new Timetable.Renderer(timetable);
    rend2.draw('#tomorrow');
</script>
