<?php
/**
 * Created by PhpStorm.
 * User: josu
 * Date: 18/09/2019
 * Time: 13:25
 */ ?>

<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="description" content="">

    <meta name="author" content="">

    <title>PMU Madagascar</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!--    MDB Core CSS-->
    <link href="assets/vendor/mdb/css/mdb.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/modern-business.css" rel="stylesheet">
    <link href="assets/vendor/timetable/css/timetablejs.css" rel="stylesheet">
</head>
