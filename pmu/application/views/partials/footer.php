<?php
/**
 * Created by PhpStorm.
 * User: josu
 * Date: 18/09/2019
 * Time: 13:25
 */ ?>



<!-- Footer -->
<footer class="py-3 bg-pmu-green">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; PMU Madagascar <?= date("Y") ?></p>
    </div>
    <!-- /.container -->
</footer>
