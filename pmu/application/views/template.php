<?php
/**
 * Created by PhpStorm.
 * User: josu
 * Date: 18/09/2019
 * Time: 13:24
 */ ?>

<!doctype html>
<html>
<?php include("partials/head.php"); ?>
<body class="wrapper">

<?php include("partials/header.php"); ?>
<main class="wrapper-content">
    <?php include("content/" . $page . ".php"); ?>
</main>
<?php include("partials/footer.php"); ?>
<?php include("partials/foot-js.php"); ?>
</body>
</html>
