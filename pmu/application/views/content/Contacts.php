<!-- Page Content -->
<div class="container">

    <!-- Page Heading/Breadcrumbs -->
    <h1 class="mt-4 mb-3">Envoyez
        <small>nous un message</small>
    </h1>

    <!-- Content Row -->
    <div class="row">
        <!-- Map Column -->
        <div class="col-lg-8 mb-4">
            <form method="get" action="<?= base_url('/contacts/sendmail') ?>" name="sentMessage" id="contactForm" novalidate>
                <div class="control-group form-group">
                    <div class="controls">
                        <label>Nom:</label>
                        <input type="text" class="form-control" id="name" name="nom" required data-validation-required-message="Please enter your name.">
                        <p class="help-block"></p>
                    </div>
                </div>
                <div class="control-group form-group">
                    <div class="controls">
                        <label>T&eacute;l&eacute;phone:</label>
                        <input type="tel" class="form-control" id="phone"  name="telephone" required data-validation-required-message="Please enter your phone number.">
                    </div>
                </div>
                <div class="control-group form-group">
                    <div class="controls">
                        <label>Adresse email:</label>
                        <input type="email" class="form-control" id="email" name="email" required data-validation-required-message="Please enter your email address.">
                    </div>
                </div>
                <div class="control-group form-group">
                    <div class="controls">
                        <label>Message:</label>
                        <textarea rows="10" cols="100" class="form-control" id="message"  name="message" required data-validation-required-message="Please enter your message" maxlength="999" style="resize:none"></textarea>
                    </div>
                </div>
                <div id="success"></div>
                <!-- For success/fail messages -->
                <button type="submit" class="btn btn-dark-green" id="sendMessageButton">Envoyer Message</button>
            </form>
        </div>
        <!-- Contact Details Column -->
        <div class="col-lg-4 mt-4">
            <h3>D&eacute;tails du contact</h3>
            <p>
                <abbr>T&eacute;l&eacute;phone</abbr>: +261 22 381 53
            </p>
            <p>
                <abbr>Email</abbr>:
                <a href="mailto:contact@pmumadagascar.mg">contact@pmumadagascar.mg
                </a>
            </p>
        </div>
    </div>
    <!-- /.row -->

</div>
