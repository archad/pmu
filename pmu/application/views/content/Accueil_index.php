<header>
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <!-- Slide One - Set the background image for this slide in the line below -->
            <!--            <div class="carousel-item active" style="background-image: url('http://placehold.it/1900x1080')">-->
            <div class="carousel-item active" style="background-image: url('assets/img/thumb-1920-523733.jpg')">
                <div class="carousel-caption d-none d-block bg-pmu-green col-md-6 col-sm-9 m-auto card px-4">
                    <h3 class="mb-4">Tirelire</h3>
                    <p class="text-black">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
            </div>
            <!-- Slide Two - Set the background image for this slide in the line below -->
            <div class="carousel-item"
                 style="background-image: url('assets/img/discover-the-great-horse-races-of-mauritius-photo-2-1920.jpg')">
                <div class="carousel-caption d-none d-block bg-pmu-green col-md-6 col-sm-9 m-auto card px-4">
                    <h3 class="mb-4">Tirelire</h3>
                    <p class="text-black">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
            </div>
            <!-- Slide Three - Set the background image for this slide in the line below -->
            <div class="carousel-item"
                 style="background-image: url('assets/img/illustration-courses-hippiques-de-trot_1-1562584079.jpg')">
                <div class="carousel-caption d-none d-block bg-pmu-green col-md-6 col-sm-9 m-auto card px-4">
                    <h3 class="mb-4">Tirelire</h3>
                    <p class="text-black">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</header>


<!-- Page Content -->
<div class="container">

    <h2 class="my-4">Programme et Résultats</h2>

    <div class="row mb-4 mt-4 d-flex justify-content-center" >
        <div class="col-lg-12 mb-4">
            <div class="card h-100 material-shadow">
                <h4 class="card-header bg-pmu-green text-white">Aujourd'hui <br><small><?= $d_today ?></small></h4>
                <div class="card-body">
                    <div id="today" class="timetable"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->

    <div class="row  d-flex justify-content-center">
        <div class="col-lg-6 mb-4">
            <div class="card h-100 material-shadow">
                <h4 class="card-header bg-pmu-green text-white">Résultats des courses <br><small><?= $d_yesterday ?></small></h4>
                <div class="card-body">
                    <div id="results" class="timetable"></div>
                </div>
                <script>

                </script>
                <div class="card-footer d-flex justify-content-end">
                    <a href="#" class="btn btn-primary material-shadow-2">VOIR PLUS</a>
                </div>
            </div>
        </div>

        <div class="col-lg-6 mb-4">
            <div class="card h-100 material-shadow">
                <h4 class="card-header bg-pmu-green text-white">Demain <br><small><?= $d_tomorrow ?></small></h4>
                <div class="card-body">
                    <div id="tomorrow" class="timetable"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.container -->
