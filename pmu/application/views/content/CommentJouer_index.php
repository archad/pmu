<?php /**
 * Created by PhpStorm.
 * User: josu
 * Date: 19/09/2019
 * Time: 15:00
 */ ?>
<div class="container">
    <!-- Page Heading/Breadcrumbs -->
    <h1 class="mt-4 mb-3">PRODUITS ALR
        <small>(Avant La Réunion)</small>
    </h1>

    <ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="quinteplus-tab" data-toggle="tab" href="#quinteplus" role="tab" aria-controls="quinteplus" aria-selected="true">
                <img src="<?= base_url() ?>assets/img/quinte+.png">
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="tic3-tab" data-toggle="tab" href="#tic3" role="tab" aria-controls="tic3" aria-selected="false"><img src="<?= base_url() ?>assets/img/tic3.png"></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="quarteplus-tab" data-toggle="tab" href="#quarteplus" role="tab" aria-controls="quarteplus" aria-selected="false"><img src="<?= base_url() ?>assets/img/quarte+.png"></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="tierce-tab" data-toggle="tab" href="#tierce" role="tab" aria-controls="tierce" aria-selected="false"><img src="<?= base_url() ?>assets/img/tierce.png"></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="sixte-tab" data-toggle="tab" href="#sixte" role="tab" aria-controls="sixte" aria-selected="false"><img src="<?= base_url() ?>assets/img/sixte.png"></a>
        </li>
    </ul>
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade  show active" id="quinteplus" role="tabpanel" aria-labelledby="quinteplus-tab">
            <div class="col-auto">
                <h4>Le Quinté+ a lieu 1 fois/jour :</h4>
                <table class="table table-bordered table-dark mb-1">
                    <tbody class="text-center">
                        <tr class="bg-pmu-red">
                            <th class="pt-2 pb-2">Chevaux à désigner :</th>
                            <th class="pt-2 pb-2"><span class="font-weight-bold">5</span></th>
                        </tr>
                        <tr class="bg-pmu-red">
                            <th class="pt-2 pb-2">Mise de base :</th>
                            <th class="pt-2 pb-2"><span class="font-weight-bold">4 000 Ar</span></th>
                        </tr>
                        <tr class="bg-pmu-red">
                            <th class="pt-2 pb-2">Fréquence :</th>
                            <th class="pt-2 pb-2"><span class="font-weight-bold">1x</span> par jour</th>
                        </tr>
                    </tbody>
                </table>
                <p>Au Quinté+, vous désignez 5 chevaux d'une même course, en précisant leur ordre de classement à l'arrivée.</p>
            </div>

            <div class="col-auto">
                <h4>3 façons de gagner :</h4>
                <table class="table border text-center mb-1">
                    <thead >
                        <tr class="bg-pmu-green text-white">
                            <th class=" border-right pt-2 pb-2" scope="col">Vous trouvez</th>
                            <th class="pt-2 pb-2" scope="col">Vous gagnez le rapport</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class=" border-right pt-2 pb-2">Les 5 premiers chevaux dans l'ordre</td>
                            <td class="pt-2 pb-2">Ordre</td>
                        </tr>
                        <tr>
                            <td class=" border-right pt-2 pb-2">Les 5 premiers chevaux dans le désordre</td>
                            <td class="pt-2 pb-2">Désordre</td>
                        </tr>
                        <tr>
                            <td class=" border-right pt-2 pb-2">Les 4 premiers chevaux, quel que soit l'ordre</td>
                            <td class="pt-2 pb-2">Bonus 4</td>
                        </tr>
                    </tbody>
                </table>
                <p>Trouvez les 5 chevaux dans l'Ordre et la Tirelire est à vous !</p>
            </div>

            <div class="col-auto">
                <h4>Cheval non-partant.</h4>
                <p>Un cheval peut être déclaré non-partant conformément au règlement des courses applicable.
                    Si le non-partant fait partie de votre pari Quinté+ unitaire enregistré, votre pari devient alors un Quarté+.
                </p>
                <div class="row">
                    <table class=" table border text-center">
                        <thead >
                            <tr class="bg-pmu-green text-white">
                                <th class=" border-right pt-2 pb-2" scope="col">Avec 1 non partant dans votre pari unitaire  ...</th>
                                <th class="pt-2 pb-2" scope="col">Vous gagnez</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class=" border-right pt-2 pb-2">Si vos 4 chevaux restants sont à l’arrivée</td>
                                <td class="pt-2 pb-2"><span class="font-weight-bold">2 bonus 4</span></td>
                            </tr>
                        </tbody>
                    </table>
                    <table class=" table border text-center mb-1">
                        <thead >
                            <tr class="bg-pmu-green text-white">
                                <th class=" border-right pt-2 pb-2" scope="col">Avec 2 non-partants dans votre pari unitaire  ...</th>
                                <th class="pt-2 pb-2" scope="col">Vous gagnez</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class=" border-right pt-2 pb-2">Si vos 3 chevaux restants sont à l’arrivée</td>
                                <td class="pt-2 pb-2"><span class="font-weight-bold">1 bonus 4</span></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <p>Les paris unitaires comportant 3 non-partants ou plus sont remboursés.</p>
            </div>

            <div class="col-auto">
                <h4>Annulation de votre pari</h4>
                <p>En cas de non-partant dans votre pari, vous pouvez l'annuler jusqu'à la fermeture du point de vente où vous l’avez acheté.
                </p>
            </div>

        </div>
        <div class="tab-pane fade" id="tic3" role="tabpanel" aria-labelledby="tic3-tab">
            <div class="col-auto">
                <h4>Tic 3 : Jouez un Tiercé, un Quarté+ et un Quinté+ dans un seul et même pari.</h4>
                <table class="table table-bordered table-dark">
                    <tbody class="text-center">
                        <tr class="bg-pmu-red">
                            <th class="pt-2 pb-2">Mise de base :</th>
                            <th class="pt-2 pb-2"><span class="font-weight-bold">8 000 Ar</span></th>
                        </tr>
                        <tr class="bg-pmu-red">
                            <th class="pt-2 pb-2">Fréquence</th>
                            <th class="pt-2 pb-2">
                                Sur toutes les courses proposant le Tiercé,Quarté+ et le Quinté+
                            </th>
                        </tr>
                    </tbody>
                </table>
                <p>Pensez-y ! Le <span class="font-weight-bold">Tic3</span> est un pari qui permet avec votre sélection de 5 chevaux pour le Quinté+ d’engager un Quarté+ avec les 4 premiers chevaux de votre combinaison et un Tiercé avec les 3 premiers chevaux de votre combinaison.</p>
                <p>Les règles qui s'appliquent sont les mêmes que celles qui régissent chacun de ces 3 paris. Le <span class="font-weight-bold">Tic3</span> vous permet d'additionner les gains de ces trois paris. En revanche, il n'est pas possible de jouer « Dans tous les ordres », ni de jouer en formule champs et combiné.</p>
                <p class="mb-0">
                    <span class="font-weight-bold">Exemple :</span><br>
                    Vous jouez un Tic 3 : 7 - 4 - 8 - 2 - 16<br>
                    L’arrivée de la course est la suivante : 7 - 4 - 8 - 16 - 2<br>
                    Vous gagnez :
                </p>
                <ul>
                    <li>1 fois le rapport Quinté+ dans le désordre</li>
                    <li>1 fois le rapport Quarté+ Bonus</li>
                    <li>1 fois le rapport Tiercé Ordre</li>
                </ul>
            </div>
        </div>
        <div class="tab-pane fade" id="quarteplus" role="tabpanel" aria-labelledby="quarteplus-tab">
            <div class="col-auto">
                <h4>Le Quarté+ se joue tous les jours sauf les dimanches.</h4>
                <table class="table table-bordered table-dark mb-1">
                    <tbody class="text-center">
                        <tr class="bg-pmu-quarteplus">
                            <th class="pt-2 pb-2">Chevaux à désigner :</th>
                            <th class="pt-2 pb-2"><span class="font-weight-bold">4</span></th>
                        </tr>
                        <tr class="bg-pmu-quarteplus">
                            <th class="pt-2 pb-2">Mise de base :</th>
                            <th class="pt-2 pb-2"><span class="font-weight-bold">3 000 Ar</span></th>
                        </tr>
                        <tr class="bg-pmu-quarteplus">
                            <th class="pt-2 pb-2">Fréquence :</th>
                            <th class="pt-2 pb-2"><span class="font-weight-bold">1x</span> par jour</th>
                        </tr>
                    </tbody>
                </table>
                <p class="mb-0">Au Quarté+, vous devez désigner quatre chevaux d'une même course, en précisant leur ordre de classement à l'arrivée.</p>
                <ul>
                    <li>Si vos quatre chevaux sont arrivés aux 4 premières places dans l'ordre indiqué, vous gagnez le rapport "Quarté+ dans l'ordre".</li>
                    <li>Si vous avez trouvé les 4 premiers chevaux de la course mais dans un ordre différent de celui de l'arrivée, vous gagnez le rapport "Quarté+ dans le désordre".</li>
                    <li>Si parmi les quatre chevaux que vous avez désignés, ne figurent que les 3 premiers chevaux de l'arrivée quel que soit l'ordre stipulé sur le ticket, vous gagnez le rapport "Bonus" du Quarté+.</li>
                </ul>
                <p class="mb-2">Trouvez les 4 chevaux dans l'Ordre et la Tirelire est à vous !</p>
                <table class="table border text-center">
                    <thead >
                        <tr class="bg-pmu-green text-white">
                            <th class=" border-right pt-2 pb-2" scope="col">Vous trouvez</th>
                            <th class="pt-2 pb-2" scope="col">Vous gagnez le rapport</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class=" border-right pt-2 pb-2">Les 4 premiers chevaux dans l'ordre</td>
                            <td class="pt-2 pb-2">Ordre</td>
                        </tr>
                        <tr>
                            <td class=" border-right pt-2 pb-2">Les 4 premiers chevaux dans le désordre</td>
                            <td class="pt-2 pb-2">Désordre</td>
                        </tr>
                        <tr>
                            <td class=" border-right pt-2 pb-2">Les 3 premiers chevaux, quel que soit l'ordre</td>
                            <td class="pt-2 pb-2">Bonus 3</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-auto">
                <h4>Cheval non-partant</h4>
                <p class="mb-3">
                    Un cheval peut être déclaré non-partant conformément au règlement des courses applicable.<br>
                    S’il fait partie de votre pari enregistré, voici les règles appliquées au Quarté+.
                </p>
                <table class="table border text-center mb-1">
                    <thead >
                        <tr class="bg-pmu-green text-white">
                            <th class=" border-right pt-2 pb-2" scope="col">Si vous avez joué</th>
                            <th class="border-right pt-2 pb-2" scope="col">Et que...</th>
                            <th class="pt-2 pb-2" scope="col">Vous gagnez</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class=" border-right pt-2 pb-2"><span class="font-weight-bold">1</span> non-partant parmi les 4 chevaux désignés</td>
                            <td class="border-right pt-2 pb-2">... vos 3 autres chevaux sont aux 3 premières places quel que soit l'ordre</td>
                            <td class="pt-2 pb-2"><span class="font-weight-bold">2 rapports bonus 3</span></td>
                        </tr>
                        <tr>
                            <td class=" border-right pt-2 pb-2"><span class="font-weight-bold">2</span> non-partants parmi les 4 chevaux désignés</td>
                            <td class="border-right pt-2 pb-2">... vos 2 autres chevaux sont aux 2 premières places quel que soit l'ordre</td>
                            <td class="pt-2 pb-2"><span class="font-weight-bold">1 bonus 3</span></td>
                        </tr>
                    </tbody>
                </table>
                <p>Les paris unitaires comportant 3 non-partants ou plus sont remboursés.</p>
                <h4>Annulation de votre pari</h4>
                <p>
                    En cas de non-partant dans votre pari, vous pouvez l'annuler jusqu'à la fermeture du point de vente où vous l’avez acheté.
                </p>
            </div>
        </div>
        <div class="tab-pane fade" id="tierce" role="tabpanel" aria-labelledby="tierce-tab">
            <div class="col-auto">
                <h4>Le Tiercé se joue une fois par jour.</h4>
                <table class="table table-bordered table-dark">
                    <tbody class="text-center">
                        <tr class="bg-pmu-green">
                            <th class="pt-2 pb-2">Chevaux à désigner :</th>
                            <th class="pt-2 pb-2"><span class="font-weight-bold">3</span></th>
                        </tr>
                        <tr class="bg-pmu-green">
                            <th class="pt-2 pb-2">Mise de base :</th>
                            <th class="pt-2 pb-2"><span class="font-weight-bold">2 000 Ar</span></th>
                        </tr>
                        <tr class="bg-pmu-green">
                            <th class="pt-2 pb-2">Fréquence :</th>
                            <th class="pt-2 pb-2"><span class="font-weight-bold">1x</span> par jour</th>
                        </tr>
                    </tbody>
                </table>
                <p class="mb-1">Vous devez désigner trois chevaux d’une même course, en précisant leur ordre de classement à l’arrivée.</p>
                <ul>
                    <li>Si vos trois chevaux sont arrivés aux 3 premières places dans l'ordre indiqué, vous gagnez le rapport "Tiercé dans l'ordre".</li>
                    <li>Si vous avez trouvé les 3 premiers chevaux de la course mais dans un ordre différent de celui de l'arrivée, vous gagnez le rapport "Tiercé dans le désordre".</li>
                </ul>
                <table class="table border text-center mb-2">
                    <thead >
                        <tr class="bg-pmu-green text-white">
                            <th class=" border-right pt-2 pb-2" scope="col">Vous trouvez</th>
                            <th class="pt-2 pb-2" scope="col">Vous gagnez le rapport</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class=" border-right pt-2 pb-2">Les 3 premiers chevaux dans l'ordre</td>
                            <td class="pt-2 pb-2">Ordre</td>
                        </tr>
                        <tr>
                            <td class=" border-right pt-2 pb-2">Les 3 premiers chevaux dans le désordre</td>
                            <td class="pt-2 pb-2">Désordre</td>
                        </tr>
                    </tbody>
                </table>
                <p>Trouvez les 5 chevaux dans l'Ordre et la Tirelire est à vous !</p>
                <h4>Cheval non-partant.</h4>
                <p>
                    Un cheval peut être déclaré non-partant conformément au règlement des courses applicable.<br>
                    S’il fait partie de votre pari enregistré, voici les règles appliquées au Tiercé.
                </p>
                <table class="table border text-center">
                    <thead >
                        <tr class="bg-pmu-green text-white">
                            <th class=" border-right pt-2 pb-2" scope="col">Si vous avez joué</th>
                            <th class="border-right pt-2 pb-2" scope="col">Et que...</th>
                            <th class="pt-2 pb-2" scope="col">Vous gagnez</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class=" border-right pt-2 pb-2"><span class="font-weight-bold">1</span> non-partant parmi les 3 chevaux désignés</td>
                            <td class="border-right pt-2 pb-2">... vos 2 autres chevaux sont aux 2 premières places quel que soit l'ordre</td>
                            <td class="pt-2 pb-2"><span class="font-weight-bold">1 CVT (Couplé Venant du Tiercé)</span></td>
                        </tr>
                        <tr>
                            <td class=" border-right pt-2 pb-2"><span class="font-weight-bold">2</span> non-partants parmi les 3 chevaux désignés</td>
                            <td class="border-right pt-2 pb-2">... votre autre cheval est premier de l'arrivée</td>
                            <td class="pt-2 pb-2"><span class="font-weight-bold">1 SVT (Simple Venant du Tiercé)</span></td>
                        </tr>
                        <tr>
                            <td class=" border-right pt-2 pb-2"><span class="font-weight-bold">2</span> non-partants parmi les 3 chevaux désignés</td>
                            <td class="border-right pt-2 pb-2"></td>
                            <td class="pt-2 pb-2"><span class="font-weight-bold">Remboursement</span></td>
                        </tr>
                    </tbody>
                </table>
                <h4>Annulation de votre pari</h4>
                <p>En cas de non-partant dans votre pari, vous pouvez l'annuler jusqu'à la fermeture du point de vente où vous l’avez acheté.</p>
            </div>
        </div>
        <div class="tab-pane fade" id="sixte" role="tabpanel" aria-labelledby="sixte-tab">
            <div class="col-auto">
                <h4>Le Sixté se joue une fois par jour.</h4>
                <table class="table table-bordered table-dark">
                    <tbody class="text-center">
                        <tr class="bg-pmu-sixte">
                            <th class="pt-2 pb-2">Chevaux à désigner :</th>
                            <th class="pt-2 pb-2"><span class="font-weight-bold">6</span></th>
                        </tr>
                        <tr class="bg-pmu-sixte">
                            <th class="pt-2 pb-2">Mise de base :</th>
                            <th class="pt-2 pb-2"><span class="font-weight-bold">3 000 Ar</span></th>
                        </tr>
                        <tr class="bg-pmu-sixte">
                            <th class="pt-2 pb-2">Fréquence :</th>
                            <th class="pt-2 pb-2"><span class="font-weight-bold">1x</span> par jour</th>
                        </tr>
                    </tbody>
                </table>
                <p class="mb-1">Vous devez désigner trois chevaux d’une même course de 10 à 14 partants, en précisant leur ordre de classement à l’arrivée.</p>
                <ul>
                    <li>Si vos six chevaux sont arrivés aux 6 premières places dans l'ordre indiqué, vous gagnez le rapport "Sixté dans l'ordre".</li>
                    <li>Si vous avez trouvé les 6 premiers chevaux de la course mais dans un ordre différent de celui de l'arrivée, vous gagnez le rapport "Sixté dans le désordre".</li>
                </ul>
                <table class="table border text-center mb-2">
                    <thead >
                        <tr class="bg-pmu-green text-white">
                            <th class=" border-right pt-2 pb-2" scope="col">Vous trouvez</th>
                            <th class="pt-2 pb-2" scope="col">Vous gagnez le rapport</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class=" border-right pt-2 pb-2">Les 6 premiers chevaux dans l'ordre</td>
                            <td class="pt-2 pb-2">Ordre</td>
                        </tr>
                        <tr>
                            <td class=" border-right pt-2 pb-2">Les 6 premiers chevaux dans le désordre</td>
                            <td class="pt-2 pb-2">Désordre</td>
                        </tr>
                    </tbody>
                </table>
                <p>Trouvez les 6 chevaux dans l'Ordre et la Tirelire est à vous !</p>
                <h4>Cheval non-partant.</h4>
                <p>
                    Un cheval peut être déclaré non-partant conformément au règlement des courses applicable.<br>
                    S’il fait partie de votre pari enregistré, voici les règles appliquées au Tiercé.
                </p>
                <table class="table border text-center">
                    <thead >
                        <tr class="bg-pmu-green text-white">
                            <th class=" border-right pt-2 pb-2" scope="col">Si vous avez joué</th>
                            <th class="border-right pt-2 pb-2" scope="col">Et que...</th>
                            <th class="pt-2 pb-2" scope="col">Vous gagnez</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class=" border-right pt-2 pb-2"><span class="font-weight-bold">1</span> non-partant parmi les 6 chevaux désignés</td>
                            <td class="border-right pt-2 pb-2">... vos 5 autres chevaux sont aux 5 premières places de l'arrivée </td>
                            <td class="pt-2 pb-2"><span class="font-weight-bold">1 QVS (Quinté Venant du Sixté)</span></td>
                        </tr>
                        <tr>
                            <td class=" border-right pt-2 pb-2"><span class="font-weight-bold">2</span> non-partants parmi les 6 chevaux désignés</td>
                            <td class="border-right pt-2 pb-2">... vos 4 autres chevaux sont aux 4 premières places de l'arrivée</td>
                            <td class="pt-2 pb-2"><span class="font-weight-bold">1 KVS (Quarté Venant du Sixté)</span></td>
                        </tr>
                        <tr>
                            <td class=" border-right pt-2 pb-2"><span class="font-weight-bold">2</span> non-partants parmi les 3 chevaux désignés</td>
                            <td class="border-right pt-2 pb-2"></td>
                            <td class="pt-2 pb-2"><span class="font-weight-bold">Remboursement</span></td>
                        </tr>
                    </tbody>
                </table>
                <h4>Annulation de votre pari</h4>
                <p>En cas de non-partant dans votre pari, vous pouvez l'annuler jusqu'à la fermeture du point de vente où vous l’avez acheté.</p>
            </div>
        </div>
    </div>

    <h1 class="mt-4 mb-3">PRODUITS PLR
        <small>(Pendant La Réunion)</small>
    </h1>

    <ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="quinte-tab" data-toggle="tab" href="#quinte" role="tab" aria-controls="quinte" aria-selected="true"><img src="<?= base_url() ?>assets/img/quinte.png"></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="quadruplet-tab" data-toggle="tab" href="#quadruplet" role="tab" aria-controls="quadruplet" aria-selected="false"><img src="<?= base_url() ?>assets/img/quadruplet.png"></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="triplet-tab" data-toggle="tab" href="#triplet" role="tab" aria-controls="triplet" aria-selected="false"><img src="<?= base_url() ?>assets/img/triplet.png"></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="jumele-tab" data-toggle="tab" href="#jumele" role="tab" aria-controls="jumele" aria-selected="false"><img src="<?= base_url() ?>assets/img/jumele.png"></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="simple-tab" data-toggle="tab" href="#simple" role="tab" aria-controls="simple" aria-selected="false"><img src="<?= base_url() ?>assets/img/simple.png"></a>
        </li>
    </ul>
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="quinte" role="tabpanel" aria-labelledby="quinte-tab">
            <div class="col-auto">
                <h4>Le Quinté a lieu 1 fois/jour :</h4>
                <table class="table table-bordered table-dark">
                    <tbody class="text-center">
                        <tr class="bg-pmu-red">
                            <th class="pt-2 pb-2">Chevaux à désigner :</th>
                            <th class="pt-2 pb-2"><span class="font-weight-bold">5</span></th>
                        </tr>
                        <tr class="bg-pmu-red">
                            <th class="pt-2 pb-2">Mise de base :</th>
                            <th class="pt-2 pb-2"><span class="font-weight-bold">3 500 Ar</span></th>
                        </tr>
                        <tr class="bg-pmu-red">
                            <th class="pt-2 pb-2">Fréquence :</th>
                            <th class="pt-2 pb-2"><span class="font-weight-bold">1x</span> par jour</th>
                        </tr>
                    </tbody>
                </table>
                <p>Au Quinté, vous désignez 5 chevaux d'une même course, en précisant leur ordre de classement à l'arrivée.</p>
            </div>

            <div class="col-auto">
                <h4>2 façons de gagner :</h4>
                <table class="table border text-center">
                    <thead >
                        <tr class="bg-pmu-green text-white">
                            <th class=" border-right pt-2 pb-2" scope="col">Vous trouvez</th>
                            <th class="pt-2 pb-2" scope="col">Vous gagnez le rapport</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class=" border-right pt-2 pb-2">Les 5 premiers chevaux dans l'ordre</td>
                            <td class="pt-2 pb-2">Ordre</td>
                        </tr>
                        <tr>
                            <td class=" border-right pt-2 pb-2">Les 5 premiers chevaux dans le désordre</td>
                            <td class="pt-2 pb-2">Désordre</td>
                        </tr>
                    </tbody>
                </table>
                <p>Trouvez les 5 chevaux dans l'Ordre et la Tirelire est à vous !</p>
            </div>
        </div>
        <div class="tab-pane fade" id="quadruplet" role="tabpanel" aria-labelledby="quadruplet-tab">
            <div class="col-auto">
                <table class="table table-bordered table-dark">
                    <tbody class="text-center">
                        <tr class="bg-pmu-quadruplet">
                            <th class="pt-2 pb-2">Chevaux à désigner :</th>
                            <th class="pt-2 pb-2"><span class="font-weight-bold">4</span></th>
                        </tr>
                        <tr class="bg-pmu-quadruplet">
                            <th class="pt-2 pb-2">Mise de base :</th>
                            <th class="pt-2 pb-2"><span class="font-weight-bold">2 500 Ar</span></th>
                        </tr>
                        <tr class="bg-pmu-quadruplet">
                            <th class="pt-2 pb-2">Fréquence :</th>
                            <th class="pt-2 pb-2"><span class="font-weight-bold">Pour les courses d'au moins 13 partants</span></th>
                        </tr>
                    </tbody>
                </table>
                <p class="mb-1">Au Quadruplet, vous devez désigner quatre chevaux d'une même course, en précisant leur ordre de classement à l'arrivée.</p>
                <ul>
                    <li>Si vos quatre chevaux sont arrivés aux 4 premières places dans l'ordre indiqué, vous gagnez le rapport "Quadruplet dans l'ordre".</li>
                    <li>Si vous avez trouvé les 4 premiers chevaux de la course mais dans un ordre différent de celui de l'arrivée, vous gagnez le rapport "Quadruplet dans le désordre".</li>
                </ul>
                <table class="table border text-center">
                    <thead >
                        <tr class="bg-pmu-green text-white">
                            <th class=" border-right pt-2 pb-2" scope="col">Vous trouvez</th>
                            <th class="pt-2 pb-2" scope="col">Vous gagnez le rapport</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class=" border-right pt-2 pb-2">Les 4 premiers chevaux dans l'ordre</td>
                            <td class="pt-2 pb-2">Ordre</td>
                        </tr>
                        <tr>
                            <td class=" border-right pt-2 pb-2">Les 4 premiers chevaux dans le désordre</td>
                            <td class="pt-2 pb-2">Désordre</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="tab-pane fade" id="triplet" role="tabpanel" aria-labelledby="triplet-tab">
            <div class="col-auto">
                <table class="table table-bordered table-dark">
                    <tbody class="text-center">
                        <tr class="bg-pmu-triplet">
                            <th class="pt-2 pb-2">Chevaux à désigner :</th>
                            <th class="pt-2 pb-2"><span class="font-weight-bold">3</span></th>
                        </tr>
                        <tr class="bg-pmu-triplet">
                            <th class="pt-2 pb-2">Mise de base :</th>
                            <th class="pt-2 pb-2"><span class="font-weight-bold">2 000 Ar</span></th>
                        </tr>
                        <tr class="bg-pmu-triplet">
                            <th class="pt-2 pb-2">Fréquence :</th>
                            <th class="pt-2 pb-2"><span class="font-weight-bold">Pour les courses d'au moins 8 partants</span></th>
                        </tr>
                    </tbody>
                </table>
                <p class="mb-1">Vous devez désigner trois chevaux d’une même course, en précisant leur ordre de classement à l’arrivée.</p>
                <ul>
                    <li>Si vos trois chevaux sont arrivés aux 3 premières places dans l'ordre indiqué, vous gagnez le rapport "Tiercé dans l'ordre".</li>
                    <li>Si vous avez trouvé les 3 premiers chevaux de la course mais dans un ordre différent de celui de l'arrivée, vous gagnez le rapport "Tiercé dans le désordre".</li>
                </ul>
                <table class="table border text-center mb-2">
                    <thead >
                        <tr class="bg-pmu-green text-white">
                            <th class=" border-right pt-2 pb-2" scope="col">Vous trouvez</th>
                            <th class="pt-2 pb-2" scope="col">Vous gagnez le rapport</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class=" border-right pt-2 pb-2">Les 3 premiers chevaux dans l'ordre</td>
                            <td class="pt-2 pb-2">Ordre</td>
                        </tr>
                        <tr>
                            <td class=" border-right pt-2 pb-2">Les 3 premiers chevaux dans le désordre</td>
                            <td class="pt-2 pb-2">Désordre</td>
                        </tr>
                    </tbody>
                </table>
                <p>Trouvez les 3 chevaux dans l'Ordre et la Tirelire est à vous !</p>
            </div>
        </div>
        <div class="tab-pane fade" id="jumele" role="tabpanel" aria-labelledby="jumele-tab">
            <div class="col-auto">
                <table class="table table-bordered table-dark">
                    <tbody class="text-center">
                        <tr class="bg-pmu-jumele">
                            <th class="pt-2 pb-2">Chevaux à désigner :</th>
                            <th class="pt-2 pb-2"><span class="font-weight-bold">2</span></th>
                        </tr>
                        <tr class="bg-pmu-jumele">
                            <th class="pt-2 pb-2">Mise de base :</th>
                            <th class="pt-2 pb-2"><span class="font-weight-bold">1 500 Ar</span></th>
                        </tr>
                        <tr class="bg-pmu-jumele">
                            <th class="pt-2 pb-2">Fréquence :</th>
                            <th class="pt-2 pb-2"><span class="font-weight-bold">toutes</span> les courses </th>
                        </tr>
                    </tbody>
                </table>
                <ul>
                    <li><span class="font-weight-bold">Pour les courses d'au moins 8 partants</span>, au <span class="font-weight-bold">Jumelé Gagnant</span>, trouver les deux premiers chevaux de l'arrivée, quel que soit l'ordre.</li>
                    <li><span class="font-weight-bold">Pour les courses d'au moins 8 partants</span>, au <span class="font-weight-bold">Jumelé Placé</span>, trouver deux des trois premiers chevaux de l'arrivée, quel que soit l'ordre.</li>
                    <li><span class="font-weight-bold">Pour les courses de 4 à 7 partants</span>, au <span class="font-weight-bold">Jumelé Ordre</span>, trouver les deux premiers chevaux dans l'ordre exact de l'arrivée.</li>
                </ul>
                <table class="table border text-center">
                    <thead >
                        <tr class="bg-pmu-green text-white">
                            <th class=" border-right pt-2 pb-2" scope="col">Vous jouez</th>
                            <th class="border-right pt-2 pb-2" scope="col">Vous trouvez</th>
                            <th class="pt-2 pb-2" scope="col">Vous gagnez le rapport</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class=" border-right pt-2 pb-2">Jumelé gagnant</td>
                            <td class="border-right pt-2 pb-2">Les 2 premiers chevaux</td>
                            <td class="pt-2 pb-2">jumelé Gagnant</td>
                        </tr>
                        <tr>
                            <td class=" border-right pt-2 pb-2">Jumelé placé</td>
                            <td class="border-right pt-2 pb-2">2 chevaux parmi les 3 premiers</td>
                            <td class="pt-2 pb-2">L'un des 3 rapports jumelé Placé</td>
                        </tr>
                        <tr>
                            <td class=" border-right pt-2 pb-2">Jumelé ordre</td>
                            <td class="border-right pt-2 pb-2">Les 2 premiers chevaux dans l'ordre exact d'arrivée</td>
                            <td class="pt-2 pb-2">Jumelé ordre</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="tab-pane fade" id="simple" role="tabpanel" aria-labelledby="simple-tab">
            <div class="col-auto">
                <table class="table table-bordered table-dark">
                    <tbody class="text-center">
                        <tr class="bg-pmu-simple">
                            <th class="pt-2 pb-2">Chevaux à désigner :</th>
                            <th class="pt-2 pb-2"><span class="font-weight-bold">1</span></th>
                        </tr>
                        <tr class="bg-pmu-simple">
                            <th class="pt-2 pb-2">Mise de base :</th>
                            <th class="pt-2 pb-2"><span class="font-weight-bold">1 000 Ar</span></th>
                        </tr>
                        <tr class="bg-pmu-simple">
                            <th class="pt-2 pb-2">Fréquence :</th>
                            <th class="pt-2 pb-2"><span class="font-weight-bold">toutes</span> les courses <span class="font-weight-bold">1</span> chance sur <span class="font-weight-bold">3</span> de gagner au Simple Placé</th>
                        </tr>
                    </tbody>
                </table>
                <p>
                    Au Simple, vous devez désigner un cheval parmi les chevaux engagés dans une épreuve.<br>
                    Si vous le jouez en Simple : vous gagnez s'il arrive <span class="font-weight-bold">premier</span><br>
                </p>
                <p class="mb-1">Si vous le jouez en  Simple</p>
                <ul>
                    <li>vous gagnez s'il arrive parmi les <span class="font-weight-bold">3 premiers à l'arrivée</span> dans une course comptant <span class="font-weight-bold">au minimum 8 chevaux</span> inscrits au programme</li>
                    <li>vous gagnez s'il arrive à la <span class="font-weight-bold">première</span> ou à la <span class="font-weight-bold">deuxième</span> place dans une course comptant <span class="font-weight-bold">entre 4 et 7 chevaux</span> inscrits au programme *</li>
                </ul>
            </div>
        </div>
    </div>

</div>
